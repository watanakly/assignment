@extends('layout')
@section('content')
<div class="content-wrapper" id="add_customer" >
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card" style="padding-left: 10px; padding-top: 20px;">
                <h3 style="text-transform: uppercase">Add New Customer</h3>
                <form action="/customerpage/add_record" method="POST" id="form">
                    {{ csrf_field() }}
                    <label for="title"> Customer Name: <span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="name" name="txt_name" style="width: 20%" placeholder="Please input Name" class=" form-control"><br>
                    <lable id="error_title" style="color: red"></lable><br>
                    <label for="body"> Phone:<span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="phone" name="txt_phone" style="width: 30%" placeholder="Please input Phone" class="form-control"><br>
                    <lable id="error_body" style="color: red;"></lable><br>
                    <label for="detail"> Email:<span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="phone" name="txt_email" style="width: 30%" placeholder="Please input Email" class="form-control"><br>
                    <lable id="error_detail" style="color: red"></lable><br>
                    <label for="detail"> Address:<span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="address" name="txt_address" style="width: 30%" placeholder="Please input Address" class="form-control"><br>
                    <lable id="error_detail" style="color: red"></lable><br>
                    <input type="submit" class="btn btn-primary" value="Save" style="background-color: #1f648b; text-transform: uppercase; font-weight: bold">
                    <a class="btn btn-danger" href="/customerpage/customer" style="text-transform: uppercase; font-weight: bold">Canel</a><br>
                </form><br>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
@endsection

@section('script')
<script src="{{mix('js/customer/customer.js')}}"></script>
@endsection
