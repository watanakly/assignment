@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card" style="padding-left: 20px;">
                <h3 style="text-transform: uppercase; padding-top: 10px">Update Customer</h3>
                <form action="/customerpage/update_record" method="GET" id="form">
                    {{ csrf_field() }}
                    @foreach($data as $item)
                        <input type="hidden" value="{{$item->id}}" name="id">
                        <label for="title"> Customer Name: <span style="color: red; font-size: 20px;">*</span></label>
                        <input type="text" value="{{$item->name}}" id="name" name="txt_name" style="width: 20%" placeholder="Please input Name" class=" form-control"><br>
                        <lable id="error_title" style="color: red"></lable><br>
                        <label for="body"> Phone:<span style="color: red; font-size: 20px;">*</span></label>
                        <input type="text" value="{{$item->phone}} " id="phone" name="txt_phone" style="width: 30%" placeholder="Please input Phone" class="form-control"><br>
                        <lable id="error_body" style="color: red;"></lable><br>
                        <label for="detail"> Email:<span style="color: red; font-size: 20px;">*</span></label>
                        <input type="text" value="{{$item->email}}" id="phone" name="txt_email" style="width: 30%" placeholder="Please input Email" class="form-control"><br>
                        <lable id="error_detail" style="color: red"></lable><br>
                        <label for="detail"> Address:<span style="color: red; font-size: 20px;">*</span></label>
                        <input type="text" value="{{$item->address}}" id="address" name="txt_address" style="width: 30%" placeholder="Please input Address" class="form-control"><br>
                        <lable id="error_detail" style="color: red"></lable><br>
                        <input type="submit" class="btn btn-primary" value="Save" style="background-color: #1f648b; text-transform: uppercase; font-weight: bold">
                        <a class="btn btn-danger" href="/customerpage/customer" style="text-transform: uppercase; font-weight: bold">Canel</a><br>
                    @endforeach
                </form><br>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
@endsection
