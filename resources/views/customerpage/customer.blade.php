@extends('layout')
@section('content')
<!-- Delete modal popup -->
<div class="modal fade" id="deletemodal">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
             <h5 class="modal-title" id="exampleModalLabel"> Delete Customer Data</h5>
             <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
             </button>
          </div>
          <form method="POST" action="/customerpage/update_customer/">
            <div class="modal-body">
              <input type="hidden" name="delete_id" id="delete_id">
              <h4>Are you sure that you want to delete?</h4>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button type="submit" name="deletedata" class="btn btn-primary">Delete</button>
            </div>
          </form>
      </div>
  </div>
</div>
<!-- End Delete modal popup -->
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="text-transform: uppercase; font-weight:bold">Customers Listing</h3>
                <a href="{{url('/customerpage/add_customer')}}" class="btn btn-default" style="margin-left: 90%; text-transform: uppercase; font-weight: bold"><span class="glyphicon glyphicon-plus"></span> Add New</a>
            </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead class="text-primary">
                  <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Email
                    </th>
                    <th>
                        Phone Number
                    </th>
                    <th>
                        Address
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Created_At
                    </th>
                    <th>
                        Updated_At
                    </th>
                    <th>
                        Action
                    </th>
                    </thead>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $item)
                    <tr>
                            <td>
                                {{ $data->firstItem() + $loop->index }}
                            </td>
                            <td>
                                {{$item->name}}
                            </td>
                            <td>
                                {{$item->email}}
                            </td>
                            <td>
                                {{$item->phone}}
                            </td>
                            <td>
                                {{$item->address}}
                            </td>
                            <td>
                                @if ($item->status == '1')
                                <a class ="btn btn-success" href="{{url('/customerpage/status_update/'.$item->id)}}">Active</a>
                                @else
                                  <a href="{{url('/customerpage/status_update/'.$item->id)}}" class ="btn btn-success">Inactive</a> 
                                @endif
                            </td>
                            <td>
                                {{$item->created_at->format('m-d-Y')}}
                            </td>
                            <td>
                                {{$item->updated_at->format('m-d-Y')}}
                            </td>
                            <td>
                                <a id="btn_edit" href="{{url('/customerpage/update_customer/'.$item->id)}}" class="btn btn-icon btn-sm btn-outline-primary" name="btn_edit"​><i class="fa fa-edit"></i></a>
                                <a id="btn_delete" class="btn btn-icon btn-sm btn-outline-danger"><i class="fa fa-trash"></i></a>
                            </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <script>
    $(document).ready(function(){
      $('#btn_delete').on('click', function(){
        $('#deletemodal').modal('show');
        
      });
    });
  </script>
@endsection
