@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card" style="padding-left: 20px;">
                <h3 style="text-transform: uppercase">Add New Item</h3>
                <form action="/itempage/update_record" method="GET" id="form">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{$data->item_id}}" name="id">
                    <label for="title"> Item Name: <span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text"  value="{{$data->item_name}}" id="name" name="name" style="width: 20%" placeholder="Please input Name" class=" form-control"><br>
                    <lable id="error_title" style="color: red"></lable>
                    <label for="body"> Description:<span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" value="{{$data->description}}" name="desc" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Sale Price</label>
                    <input type="text"  value="{{$data->sale_price}}" name="sale_price" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Purchase Price Price</label>
                    <input type="text" value="{{$data->purchase_price}}" name="purchase_price" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Quantity</label>
                    <input type="text" value="{{$data->quantity}}" name="qty" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Choose Category</label>
                    <select name="category_id" id="category_id", class="form-control" style="width: 30%">
                        @foreach($categories as $key => $category)
                            <option value="{{$category->cate_id}}" @if($data->cate_id == $category->cate_id)selected @endif>{{$category->category_name}}</option>
                        @endforeach
                    </select><br>
                    <input type="submit" class="btn btn-primary" value="Save" style="background-color: #1f648b; text-transform: uppercase; font-weight: bold">
                    <a class="btn btn-danger" href="/categorypage/category" style="text-transform: uppercase; font-weight: bold">Canel</a><br>
                </form><br>
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
@endsection
