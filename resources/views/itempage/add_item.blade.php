@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card" style="padding-left: 10px; padding-top: 20px;">
                <h3 style="text-transform: uppercase">Add New Item</h3>
                <form action="/itempage/add_record" method="POST" id="form">
                    {{ csrf_field() }}
                    <label for="title"> Item Name: <span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="name" name="name" style="width: 20%" placeholder="Please input Name" class=" form-control"><br>
                    <lable id="error_title" style="color: red"></lable>
                    <label for="body"> Description:<span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text"  name="desc" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Sale Price</label>
                    <input type="text" name="sale_price" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Purchase Price Price</label>
                    <input type="text" name="purchase_price" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Quantity</label>
                    <input type="text" name="qty" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Choose Category</label>
                    <select name="category_id" id="category_id", class="form-control" style="width: 30%"><br>
                        <option value="">-- Select Category --</option>
                            @foreach($categories as $cate_id => $category_name)
                                <option value="{{$cate_id}}">{{$category_name}}</option>
                            @endforeach
                    </select><br>
                    <input type="submit" class="btn btn-primary" value="Save" style="background-color: #1f648b; text-transform: uppercase; font-weight: bold">
                    <a class="btn btn-danger" href="/categorypage/category" style="text-transform: uppercase; font-weight: bold">Canel</a><br>
                </form><br>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
<script>
    $(function() {
        $('#toggle').bootstrapToggle({
            on: 'Enabled',
            off: 'Disabled'
        });
    })
</script>
@endsection
