@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="text-transform: uppercase; font-weight:bold">Items Listing</h3>
                <a href="{{url('/itempage/add_item')}}" class="btn btn-default" style="margin-left: 90%; text-transform: uppercase; font-weight: bold"><span class="glyphicon glyphicon-plus"></span> Add New</a>
            </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead class=" text-primary">
                    <th>
                        ID
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Sale Price
                    </th>
                    <th>
                        Purchase Price
                    </th>
                    <th>
                        Quantity
                    </th>
                    <th>
                        Category Name
                    </th>
                    <th>
                        Action
                    </th>
                    </thead>
                    <tbody>
                    @foreach($data as $item)
                        <tr>
                            <td>
                                {{ $data->firstItem() + $loop->index }}
                            </td>
                            <td>
                                {{$item->item_name}}
                            </td>
                            <td>
                                {{$item->sale_price}}
                            </td>
                            <td>
                                {{$item->purchase_price}}
                            </td>
                            <td>
                                {{$item->quantity}}
                            </td>
                            <td>
                                {{$item->category->category_name}}
                            </td>
                            <td>
                                <a href="{{url('/itempage/update_item/'.$item->item_id)}}" class="btn btn-primary"><span class=""></span> EDIT</a>
                                <a onclick="return confirm('Are you sure that you want to delete?')"  href="{{url('/itempage/delete/'.$item->item_id)}}" class="btn btn-danger"><span class=""></span> DELETE</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{$data->links()}}
            </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
@endsection
