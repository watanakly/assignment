@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card" style="padding-left: 20px;">
                <h3 style="text-transform: uppercase">Update Category</h3>
                <form action="/categorypage/update_record" method="GET" id="form">
                    {{ csrf_field() }}
                    @foreach($data as $item)
                        <input type="hidden" value="{{$item->cate_id}}" name="id">
                        <label for="title"> Category Name: <span style="color: red; font-size: 20px;">*</span></label>
                        <input type="text" id="name" name="txt_name" style="width: 20%" placeholder="Please input Name" class=" form-control" value="{{$item->category_name}}"><br>
                        <lable id="error_title" style="color: red"></lable>
                        <label for="body"> Description:<span style="color: red; font-size: 20px;">*</span></label>
                        <input type="text" id="phone" name="txt_desc" style="width: 30%" placeholder="Please input Description" class="form-control" value="{{$item->description}}"><br>
                        <label>Status</label>
                        <input type="checkbox" name="txt_status" data-toggle="toggle" data-on="Enabled" data-off="Disabled" id="toggle" value="{{$item->enabled}}">
                        <input type="submit" class="btn btn-primary" value="Save" style="background-color: #1f648b; text-transform: uppercase; font-weight: bold">
                        <a class="btn btn-danger" href="/categorypage/category" style="text-transform: uppercase; font-weight: bold">Cancel</a><br>
                    @endforeach
                </form><br>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
@endsection
