@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="text-transform: uppercase; font-weight:bold">Category Listing</h3>
                <a href="{{url('/categorypage/add_category')}}" class="btn btn-default" style="margin-left: 90%; text-transform: uppercase; font-weight: bold"><span class="glyphicon glyphicon-plus"></span> Add New</a>
            </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead class=" text-primary">
                    <th>
                        ID
                    </th>
                    <th>
                        Name
                    </th>
                    <th>
                        Description
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                        Created_At
                    </th>
                    <th>
                        Updated_At
                    </th>
                    <th>
                        Action
                    </th>
                    </thead>
                    <tbody>
                    @foreach($data as $item)
                    <tr>
                            <td>
                                {{ $data->firstItem() + $loop->index }}
                            </td>
                            <td>
                                {{$item->category_name}}
                            </td>
                            <td>
                                {{$item->description}}
                            </td>
                            <td>
                                {{$item->enabled}}
                            </td>
                            <td>
                                {{$item->created_at->format('m-d-Y')}}
                            </td>
                            <td>
                                {{$item->updated_at->format('m-d-Y')}}
                            </td>
                            <td>
                                <a href="{{url('/categorypage/update_category/'.$item->cate_id)}}" class="btn btn-primary"><span class=""></span> EDIT</a>
                                <a onclick="return confirm('Are you sure that you want to delete?')"  href="{{url('/categorypage/delete/'.$item->cate_id)}}" class="btn btn-danger"><span class=""></span> DELETE</a>
                            </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="dataTables_info" id="example1_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div>
                {{$data->links()}}
            </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
@endsection
