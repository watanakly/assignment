@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card" style="padding-left: 10px; padding-top: 20px;">
                <h3 style="text-transform: uppercase">Add New Category</h3>
                <form action="/categorypage/add_record" method="POST" id="form">
                    {{ csrf_field() }}
                    <label for="title"> Category Name: <span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="name" name="txt_name" style="width: 20%" placeholder="Please input Name" class=" form-control"><br>
                    <lable id="error_title" style="color: red"></lable>
                    <label for="body"> Description:<span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="phone" name="txt_desc" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Status</label>
                    <input type="checkbox" name="txt_status" data-toggle="toggle" data-on="Enabled" data-off="Disabled" id="toggle">
                    <input type="submit" class="btn btn-primary" value="Save" style="background-color: #1f648b; text-transform: uppercase; font-weight: bold">
                    <a class="btn btn-danger" href="/categorypage/category" style="text-transform: uppercase; font-weight: bold">Canel</a><br>
                </form><br>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
<script>
    $(function() {
        $('#toggle').bootstrapToggle({
            on: 'Enabled',
            off: 'Disabled'
        });
    })
</script>
@endsection
