@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card" style="padding-left: 20px;">
                <h3 style="text-transform: uppercase; padding-top:20px">Update Invoice</h3>
                <form action="/salepage/invoiceitempage/update_record" method="GET" id="form">
                    {{ csrf_field() }}
                    <input type="hidden" value="{{$data->id}}" name="id">
                    <label for="title"> Invoice Number: <span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="invoice_number" value="{{$data->invoice_id}}" name="invoice_number" style="width: 20%" placeholder="Please input Name" class=" form-control"><br>
                    <label>Quantity</label>
                    <input type="text" name="quantity" value="{{$data->quantity}}" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Price</label>
                    <input type="text" name="price" value="{{$data->price}}" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Total</label>
                    <input type="text" name="total" value="{{$data->total}}" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Choose Item</label>
                    <select name="item_id" id="item_id", class="form-control" style="width: 30%">
                        @foreach($items as $key => $item)
                            <option value="{{$item->item_id}}" @if ($data->id == $item->id)selected @endif>{{$item->item_name}}</option>
                        @endforeach
                    </select><br>
                    <input type="submit" class="btn btn-primary" value="Save" style="background-color: #1f648b; text-transform: uppercase; font-weight: bold">
                    <a class="btn btn-danger" href="/salepage/invoiceitempage/invoice_item" style="text-transform: uppercase; font-weight: bold">Canel</a><br>
                </form><br>
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
<script type="text/javascript">
    $('.invoice_at').datepicker({
       format: 'yyyy-mm-dd'
     });
</script>
@endsection
