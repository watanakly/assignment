@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="text-transform: uppercase; font-weight:bold">Invoice Listing</h3>
                <a href="{{url('/salepage/invoiceitempage/add_invoice_item')}}" class="btn btn-default" style="margin-left: 90%; text-transform: uppercase; font-weight: bold"><span class="glyphicon glyphicon-plus"></span> Add New</a>
            </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead class="text-primary">
                  <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Invoice Number
                    </th>
                    <th>
                        Item Name
                    </th>
                    <th>
                        Quantity
                    </th>
                    <th>
                        Price
                    </th>
                    <th>
                        Total
                    </th>
                    <th>
                        Created_At
                    </th>
                    <th>
                        Action
                    </th>
                    </thead>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $item)
                    <tr>
                            <td>
                                {{ $data->firstItem() + $loop->index }}
                            </td>
                            <td>
                                {{$item->invoice_id}}
                            </td>
                            <td>
                                {{$item->item->item_name}}
                            </td>
                            <td>
                                {{$item->quantity}}
                            </td>
                            <td>
                                {{$item->price}}
                            </td>
                            <td>
                                {{$item->total}}
                            </td>
                            <td>
                                {{$item->created_at->format('m-d-Y')}}
                            </td>
                            <td>
                                <a href="{{url('/salepage/invoiceitempage/update_invoice_item/'.$item->id)}}" class="btn btn-primary"><span class=""></span> EDIT</a>
                                <a onclick="return confirm('Are you sure that you want to delete?')"  href="{{url('/salepage/invoiceitempage/delete/'.$item->id)}}" class="btn btn-danger"><span class=""></span> DELETE</a>
                            </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
@endsection
<!--
<div class="container">
    <h1>Laravel Bootstrap Datepicker</h1>
    <input class="date form-control" type="text">
</div>

<script type="text/javascript">
    $('.date').datepicker({
       format: 'mm-dd-yyyy'
     });
</script>
!>
