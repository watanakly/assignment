@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title" style="text-transform: uppercase; font-weight:bold">Invoice Listing</h3>
                <a href="{{url('/salepage/invoicepage/add_invoice')}}" class="btn btn-default" style="margin-left: 90%; text-transform: uppercase; font-weight: bold"><span class="glyphicon glyphicon-plus"></span> Add New</a>
            </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead class="text-primary">
                  <tr>
                    <th>
                        ID
                    </th>
                    <th>
                        Invoice Number
                    </th>
                    <th>
                        Invoice At
                    </th>
                    <th>
                        Amount
                    </th>
                    <th>
                        Currency
                    </th>
                    <th>
                        Total
                    </th>
                    <th>
                        Customer Name
                    </th>
                    <th>
                        Created_At
                    </th>
                    <th>
                        Action
                    </th>
                    </thead>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($data as $item)
                    <tr>
                            <td>
                                {{ $data->firstItem() + $loop->index }}
                            </td>
                            <td>
                                {{$item->invoice_number}}
                            </td>
                            <td>
                                {{$item->invoiced_at->format('Y-m-d')}}
                            </td>
                            <td>
                                {{$item->amount}}
                            </td>
                            <td>
                                {{$item->currency}}
                            </td>
                            <td>
                                {{$item->total}}
                            </td>
                            <td>
                                {{$item->customer->name}}
                            </td>
                            <td>
                                {{$item->created_at->format('m-d-Y')}}
                            </td>
                            <td>
                                <a href="{{url('/salepage/invoicepage/update_invoice/'.$item->id)}}" class="btn btn-primary"><span class=""></span> EDIT</a>
                                <a onclick="return confirm('Are you sure that you want to delete?')"  href="{{url('/salepage/invoicepage/delete/'.$item->id)}}" class="btn btn-danger"><span class=""></span> DELETE</a>
                            </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
@endsection
<!--
<div class="container">
    <h1>Laravel Bootstrap Datepicker</h1>
    <input class="date form-control" type="text">
</div>

<script type="text/javascript">
    $('.date').datepicker({
       format: 'mm-dd-yyyy'
     });
</script>
!>
