@extends('layout')
@section('content')
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
            <div class="card" style="padding-left: 20px;">
                <h3 style="text-transform: uppercase; padding-top:20px">Update Invoice</h3>
                <form action="/salepage/invoicepage/add_record" method="POST" id="form">
                    {{ csrf_field() }}
                    <label for="title"> Invoice Number: <span style="color: red; font-size: 20px;">*</span></label>
                    <input type="text" id="invoice_number" name="invoice_number" style="width: 20%" placeholder="Please input Name" class=" form-control"><br>
                    <label for="body"> Invoiced At:<span style="color: red; font-size: 20px;">*</span></label><br>
                    <input type="text" class="invoice_at form-control" name="invoiced_at" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Amount</label>
                    <input type="text" name="amount" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Currency</label>
                    <input type="text" name="currency" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Total</label>
                    <input type="text" name="total" style="width: 30%" placeholder="Please input Description" class="form-control"><br>
                    <label>Choose Customer</label>
                    <select name="customer_id" id="customer_id", class="form-control" style="width: 30%">
                        @foreach($customers as $id => $name)
                            <option value="{{$id}}">{{$name}}</option>
                        @endforeach
                    </select><br>
                    <input type="submit" class="btn btn-primary" value="Save" style="background-color: #1f648b; text-transform: uppercase; font-weight: bold">
                    <a class="btn btn-danger" href="/salepage/invoicepage/invoice" style="text-transform: uppercase; font-weight: bold">Canel</a><br>
                </form><br>
            </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
</div>
<script type="text/javascript">
    $('.invoice_at').datepicker({
       format: 'yyyy-mm-dd'
     });
</script>
@endsection
