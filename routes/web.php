<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@login');
Route::get('/dashboard', 'DashbaordController@index');
// Customer Page
Route::get('customerpage/customer','CustomerController@index');
Route::post('customerpage/add_record','CustomerController@add');
Route::get('customerpage/add_customer','CustomerController@create_form');
Route::get('customerpage/update_record','CustomerController@update_record');
Route::get('customerpage/update_customer/{id}','CustomerController@update');
Route::get('customerpage/delete/{id}','CustomerController@delete');
Route::get('customerpage/status_update/${id}', 'CustomerController@status_update');

//Category Page
Route::get('categorypage/category','CategoryController@index');
Route::post('categorypage/add_record','CategoryController@add');
Route::get('categorypage/add_category','CategoryController@create_form');
Route::get('categorypage/update_record','CategoryController@update_record');
Route::get('categorypage/update_category/{cate_id}','CategoryController@update');
Route::get('categorypage/delete/{cate_id}','CategoryController@delete');

// Item
Route::get('itempage/item','ItemController@index');
Route::post('itempage/add_record','ItemController@add');
Route::get('itempage/add_item','ItemController@create_form');
Route::get('itempage/update_record','ItemController@update_record');
Route::get('itempage/update_item/{item_id}','ItemController@update');
Route::get('itempage/delete/{item_id}','ItemController@delete');

// Sale->Invoice
Route::get('salepage/invoicepage/invoice','InvoiceController@index');
Route::post('salepage/invoicepage/add_record','InvoiceController@add');
Route::get('salepage/invoicepage/add_invoice','InvoiceController@create_form');
Route::get('salepage/invoicepage/update_record','InvoiceController@update_record');
Route::get('salepage/invoicepage/update_invoice/{id}','InvoiceController@update');
Route::get('salepage/invoicepage/delete/{id}','InvoiceController@delete');

// Sale/InvoiceItem
Route::get('salepage/invoiceitempage/invoice_item','InvoiceItemController@index');
Route::post('salepage/invoiceitempage/add_record','InvoiceItemController@add');
Route::get('salepage/invoiceitempage/add_invoice_item','InvoiceItemController@create_form');
Route::get('salepage/invoiceitempage/update_record','InvoiceItemController@update_record');
Route::get('salepage/invoiceitempage/update_invoice_item/{id}','InvoiceItemController@update');
Route::get('salepage/invoiceitempage/delete/{id}','InvoiceItemController@delete');

Auth::routes();

