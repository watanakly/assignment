<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    protected $table = "invoice_items";
    protected $fillable = [
        'invoice_id',
        'item_id',
        'quantity',
        'price',
        'total'
    ];
    public function item()
    {
        return $this->belongsTo(Item::class,'item_id','item_id');
    }
}
