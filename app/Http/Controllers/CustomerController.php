<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function index()
    {
        $data = Customer::paginate(8);
        return view('customerpage/customer',compact('data'));
    }
    public function create_form()
    {
        return view('customerpage/add_customer');
    }
    public function add()
    {
            $data = new Customer();
            $data->name         = request('txt_name');
            $data->phone        = request('txt_phone');
            $data->email        = request('txt_email');
            $data->address      = request('txt_address');
            $data->save();
            return redirect('customerpage\customer')-> with('success','You have created Customer successfully');
    }
    public function update_record(Request $update)
    {
        $id = $update->id;
        $data= array(
            'name'      => $update->txt_name,
            'phone'     => $update->txt_phone,
            'email'     => $update->txt_email,
            'address'   => $update->txt_address
        );
        $sql= Customer::where('id', $id)->update($data);
        if($sql)
        {
            return redirect('customerpage\customer')-> with('success','You have updated Customer successfully');
        }
        else
        {
            echo ("Update Error!");
        }
    }
    public function delete($id)
    {
        Customer::findOrFail($id)->delete();
        return redirect('customerpage\customer');

    }
    public function search(Request $request)
    {
        $data = Customer::search($request);
        return view('customerpage\customer', compact('data'));

    }
    public function update($id)
    {
        $data= Customer::where('id', $id)->get();
        return view('customerpage\update_customer', compact('data'));
    }
    public function status_update($id){
        
        session()->flash('msg', 'Customer status has been updated successfully');
        return redirect('customerpage\customer');
    }
}
