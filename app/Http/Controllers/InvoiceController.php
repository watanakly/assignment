<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\Customer;
class InvoiceController extends Controller
{
    public function index()
    {
        $data = Invoice::with('customer')->paginate(4);
       return view('salepage\invoicepage\invoice', compact('data'));
    }
    public function add()
    {
        $data = new Invoice();
        $data -> invoice_number  = request('invoice_number');
        $data -> invoiced_at     = request('invoiced_at');
        $data -> amount          = request('amount');
        $data -> currency        = request('currency');
        $data -> total           = request('total');
        $data -> customer_id     = request('customer_id');
        dd($data);
        $data->save();
        return redirect('salepage/invoicepage/invoice');
    }
    public function create_form()
    {
        $customers = Customer::pluck('name','id');
        return view('salepage\invoicepage\add_invoice', compact('customers'));
    }
    public function update_record(Request $update)
    {
        $id = $update->id;
        $data = array(
            'invoice_number'   => $update->invoice_number,
            'invoiced_at'      => $update->invoiced_at,
            'amount'           => $update->amount,
            'currency'         => $update->currency,
            'total'            => $update->total,
            'customer_id'      => $update->customer_id
        );
        $sql = Invoice::where('id', $id)->update($data);
        dd($sql);
        //return redirect('salepage\invoicepage\invoice')->with('success','You have updated Invoice successfully');
    }
    public function update($id)
    {
        $data      = Invoice::where('id', $id)->first();
        $customers = Customer::get();
        return view('salepage\invoicepage\update_invoice',compact('data','customers'));
    }
    public function delete($id)
    {
        Invoice::findOrFail($id)->delete();
        return redirect('salepage/invoicepage/invoice');
    }
}
