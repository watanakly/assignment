<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $data= Category::paginate(4);
        return view('categorypage\category',compact('data'));
    }
    public function create_form()
    {
        return view('categorypage\add_category');
    }
    public function add()
    {
        $data= new Category();
        $data->category_name    = request('txt_name');
        $data->description      = request('txt_desc');
        $data->enabled          = request('txt_status');
        $data->save();
        return redirect('categorypage/category')->with('success','You have created Category successfully');
    }
    public function update($id)
    {
        $data= Category::where('cate_id', $id)->get();
        return view('categorypage\update_category', compact('data'));
    }
    public function update_record(Request $update)
    {
        $id = $update->id;
        $data = array(
            'category_name'  => $update->txt_name,
            'description'    => $update->txt_desc,
            'enabled'        => $update->txt_status
        );
        $sql= Category::where ('cate_id', $id)->update($data);
        if($sql)
        {
            return redirect('categorypage/category')-> with('success','You have updated Category successfully');
        }
        else
        {
            echo ("Update Error!");
        }
    }
    public function delete($id)
    {
        Category::findOrFail($id)->delete();
        return redirect('categorypage/category');

    }

}
