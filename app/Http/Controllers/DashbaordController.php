<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Category;
use App\Item;
use App\Invoice;
use App\InvoiceItem;
class DashbaordController extends Controller
{
    public function index()
    {
        $data = array();
        $data['CountCustomer']      = Customer::count();
        $data['CountCategory']      = Category::count();
        $data['CountItem']          = Item::count();
        $data['CountInvoice']       = Invoice::count();
        $data['CountInvoiceItem']   = InvoiceItem::count();
        return view('dashboard', compact('data'));
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
}
