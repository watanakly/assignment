<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use App\Item;
use App\Category;
use Intervention\Image\Facades\Image;
class ItemController extends Controller
{
    public function index()
    {
        $data = Item::with('category')->paginate(4);
       return view('itempage\item', compact('data'));
    }
    public function add()
    {
        $data= new Item();

        $data->item_name        = request('name');
        $data->description      = request('desc');
        $data->sale_price       = request('sale_price');
        $data->purchase_price   = request('purchase_price');
        $data->quantity         = request('qty');
        $data->cate_id          = request('category_id');
        //dd($data);
        $data->save();
        return redirect('itempage/item')->with('success','You have created Item successfully');
    }
    public function create_form()
    {
        $categories = Category::pluck('category_name','cate_id');
        return view('itempage/add_item', compact('categories'));
    }
    public function update($id)
    {
        $data       = Item::where('item_id',$id)->first();
        $categories = Category::get();
        return view('itempage/update_item', compact('data','categories'));
    }
    public function update_record(Request $update)
    {
        $id   = $update->id;
        $data = array(
            'item_name'         => $update->name,
            'description'       => $update->desc,
            'sale_price'        => $update->sale_price,
            'purchase_price'    => $update->purchase_price,
            'quantity'          => $update->qty,
            'cate_id'           => $update->category_id
        );
        $sql = Item::where('item_id', $id)->update($data);
        //dd($sql);
        return redirect('itempage/item')->with('success','You have updated Item successfully');
    }
    public function delete($id)
    {
        Item::findOrFail($id)->delete();
        return redirect('itempage/item');
    }

}
