<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\InvoiceItem;
class InvoiceItemController extends Controller
{
    public function index()
    {
        $data = InvoiceItem::with('item')->paginate(4);
        return view('salepage\invoiceitempage\invoice_item', compact('data'));
    }
    public function create_form()
    {
        $items = Item::pluck('item_name','item_id');
        return view('salepage/invoiceitempage/add_invoice_item', compact('items'));
    }
    public function add()
    {
        $data = new InvoiceItem();
        $data -> invoice_id     = request('invoice_number');
        $data -> quantity       = request('quantity');
        $data -> price          = request('price');
        $data -> total           = request('total');
        $data -> item_id        = request('item_id');
        //dd($data);
        $data -> save();
        return redirect('salepage/invoiceitempage/invoice_item');
    }
    public function update_record(Request $update)
    {
        $id = $update->id;
        $data = array(
            'invoice_id'    => $update->invoice_number,
            'quantity'      => $update->quantity,
            'price'         => $update->price,
            'total'         => $update->total,
            'item_id'       => $update->item_id
        );
        $sql = InvoiceItem::where('id', $id)->update($data);
        //dd($data);
        return redirect('salepage/invoiceitempage/invoice_item')->with('sucess','You have updated Invoice Item successfully');
    }
    public function update($id)
    {
        $data = InvoiceItem::where('id', $id)->first();
        $items = Item::get();
        return view('salepage/invoiceitempage/update_invoice_item',compact('data','items'));
    }
    public function delete($id)
    {
        InvoiceItem::findOrFail($id)->delete();
        return redirect('salepage/invoiceitempage/invoice_item');
    }
}
