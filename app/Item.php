<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $table         = 'items';
    protected $fillable   = ['item_name, description','sale_price','purchase_price','qty'];
    protected $primaryKey = 'item_id';

    public function category(){
        return $this->belongsTo(Category::class, 'cate_id', 'cate_id');
    }
    public function invoice_item()
    {
        return $this->hasMany(InvoiceItem::class);
    }
}
