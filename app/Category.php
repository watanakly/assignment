<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    //use SoftDeletes;
    public $table       = 'categories';
    protected $fillable = ['name','description'];

    public function items(){
        return $this->hasMany(Item::class);
    }
}
