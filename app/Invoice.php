<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    public $table       ="invoices";
    protected $fillable = [
        'invoice_numner',
        'invoice_at',
        'currency',
        'total',
        'customer_id',
        'deleted_at',
        'updated_at',
        'created_at'
    ];
    protected $dates = ['invoiced_at'];
    public function customer(){
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

}
