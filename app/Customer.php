<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    //use SoftDeletes;
    public $table       = 'customers';
    protected $fillable = [
        'name',
        'phone',
        'email',
        'address',
    ];
    public static function search($request){
        $search = $request->get('search');
        $list   = self::query();
        if ($search) {
            $list = $list->where('name','like','%'.$search.'%');
        }
        return $list->paginate(15);
    }
    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }
    public static function status($request){
        $customers == DB::table('customers')
                        ->select('status')
                        ->where('id','=', $id);
        if($customers->status == '1'){
            $status = '0';
        }else{
            $status = '1';
        }
        $value = array('status' => $status);
        DB::table('customers')->where('id', $id)->update($value);
    }
}
